import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChapterListPage } from './chapter-list.page';

describe('ChapterListPage', () => {
  let component: ChapterListPage;
  let fixture: ComponentFixture<ChapterListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChapterListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChapterListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
