import { TestBed } from '@angular/core/testing';

import { TestSeriesInfoService } from './test-series-info.service';

describe('TestSeriesInfoService', () => {
  let service: TestSeriesInfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestSeriesInfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
