import { DatabaseServiceService } from "./database-service.service";
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class SaveTestInfoService {
  testDetailsObj = new BehaviorSubject(null);
  lastTestDetailObj = new BehaviorSubject(null);

  constructor(private databaseServiceService: DatabaseServiceService) {
    // console.log('in constructor save test info');
    // this.databaseServiceService.getDatabaseState().subscribe(ready => {
    //   if (ready) {
    //     this.getTestDetails();
    //   }
    // })
  }

  insertIntoTable(testDetailsObj, tableName) {
    // console.log("insert into table service");
    let obj = [
      testDetailsObj.testDate,
      testDetailsObj.subjectName,
      testDetailsObj.totalQue,
      testDetailsObj.corrMarks,
      testDetailsObj.inCorrMarks,
      testDetailsObj.obtainedMarks,
      testDetailsObj.phyObtainedMarks,
      testDetailsObj.chemObtainedMarks,
      testDetailsObj.mathsObtainedMarks,
      testDetailsObj.totalMarks,
      testDetailsObj.totalTime,
      testDetailsObj.attQueCount,
      testDetailsObj.nonAttQueCount,
      testDetailsObj.corrQueCount,
      testDetailsObj.inCorrQueCount,
      testDetailsObj.timeTakenMin,
      testDetailsObj.timeTakenSec,
      testDetailsObj.testType,
      testDetailsObj.testMode,
      testDetailsObj.questions,
      testDetailsObj.chapterList,
      testDetailsObj.manualCount,
      testDetailsObj.phyStart,
      testDetailsObj.chemStart,
      testDetailsObj.mathsStart,
      testDetailsObj.testSubmitted,
      testDetailsObj.questionStartsAt,
    ];
    return this.databaseServiceService
      .getDataBase()
      .executeSql(
        `INSERT INTO ${tableName} (TEST_DATE, SUBJECT_NAME, TOTAL_QUES, CORR_MARKS, INCORR_MARKS, OBTAINED_MARKS, PHY_OBTAINED_MARKS, 
          CHEM_OBTAINED_MARKS, MATHS_OBTAINED_MARKS, TOTAL_MARKS, TOTAL_TIME, ATTEMPTED, NON_ATTEMPTED, CORR_QUE, INCORR_QUE, 
          TIME_TAKEN_MIN, TIME_TAKEN_SEC, TEST_TYPE, TEST_MODE, QUESTIONS, CHAPTER_LIST, MANUAL_COUNT, PHY_START, CHEM_START, 
          MATHS_START, TEST_SUBMITTED, QUESTION_STARTS_AT) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
        obj
      )
      .then((data) => {
        // console.log("in insert table ");
        // console.log('in save test select', data)
        // console.log('in save test select')
        // this.databaseServiceService
        //   .getDataBase()
        //   .executeSql(`SELECT * FROM ${tableName} `, [])
        //   .then((data) => {
        //     console.log("in save test select", data);
        //   });
        this.getTestDetails(tableName);
        this.getLastTestInfo(tableName);
      });
  }

  updateTable(testId, testDetailsObj, tableName) {
    let obj = [
      testDetailsObj.testDate,
      testDetailsObj.subjectName,
      testDetailsObj.totalQue,
      testDetailsObj.corrMarks,
      testDetailsObj.inCorrMarks,
      testDetailsObj.obtainedMarks,
      testDetailsObj.phyObtainedMarks,
      testDetailsObj.chemObtainedMarks,
      testDetailsObj.mathsObtainedMarks,
      testDetailsObj.totalMarks,
      testDetailsObj.totalTime,
      testDetailsObj.attQueCount,
      testDetailsObj.nonAttQueCount,
      testDetailsObj.corrQueCount,
      testDetailsObj.inCorrQueCount,
      testDetailsObj.timeTakenMin,
      testDetailsObj.timeTakenSec,
      testDetailsObj.testType,
      testDetailsObj.testMode,
      testDetailsObj.questions,
      testDetailsObj.chapterList,
      testDetailsObj.manualCount,
      testDetailsObj.phyStart,
      testDetailsObj.chemStart,
      testDetailsObj.mathsStart,
      testDetailsObj.testSubmitted,
      testDetailsObj.questionStartsAt,
    ];
    return this.databaseServiceService
      .getDataBase()
      .executeSql(
        `UPDATE ${tableName} SET TEST_DATE = ?, SUBJECT_NAME = ?, TOTAL_QUES = ?, CORR_MARKS = ?, INCORR_MARKS = ?, OBTAINED_MARKS = ?,
        PHY_OBTAINED_MARKS = ?, CHEM_OBTAINED_MARKS = ?, MATHS_OBTAINED_MARKS = ?, TOTAL_MARKS = ?, TOTAL_TIME = ?, ATTEMPTED = ?, 
        NON_ATTEMPTED = ?, CORR_QUE = ?, INCORR_QUE = ?, TIME_TAKEN_MIN = ?, TIME_TAKEN_SEC = ?, TEST_TYPE = ?, TEST_MODE = ?, 
        QUESTIONS = ?, CHAPTER_LIST = ?, MANUAL_COUNT = ?, PHY_START = ?, CHEM_START = ?, MATHS_START = ?, TEST_SUBMITTED = ?, 
        QUESTION_STARTS_AT = ? WHERE TEST_ID = ${testId}`,
        obj
      )
      .then((_) => {
        // this.databaseServiceService
        //   .getDataBase()
        //   .executeSql(`SELECT * FROM ${tableName} `, [])
        //   .then((data) => {
        //     console.log("in save test select", data);
        //   });
        this.getTestDetails(tableName);
        this.getLastTestInfo(tableName);
      });
  }

  getTestId(tableName) {
    return this.databaseServiceService
      .getDataBase()
      .executeSql(`SELECT TEST_ID FROM ${tableName} `, [])
      .then((data) => {
        let testId = data.rows.length;
        return testId;
      });
  }

  getTestDetails(tableName) {
    // console.log('in get test details service')
    return this.databaseServiceService
      .getDataBase()
      .executeSql(
        `SELECT TEST_ID, TEST_DATE, SUBJECT_NAME, OBTAINED_MARKS, TOTAL_MARKS, 
    TEST_SUBMITTED FROM ${tableName} `,
        []
      )
      .then((data) => {
        let testDetails = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            testDetails.push({
              testId: data.rows.item(i).TEST_ID,
              testDate: data.rows.item(i).TEST_DATE,
              subjectName: data.rows.item(i).SUBJECT_NAME,
              obtainedMarks: data.rows.item(i).OBTAINED_MARKS,
              totalMarks: data.rows.item(i).TOTAL_MARKS,
              testSubmitted: data.rows.item(i).TEST_SUBMITTED,
            });
          }
          this.testDetailsObj.next(testDetails);
        } else {
          this.testDetailsObj.next(testDetails);
        }
      });
  }

  getTestDetailsForTest(testId, tableName) {
    return this.databaseServiceService
      .getDataBase()
      .executeSql(`SELECT * FROM ${tableName}  WHERE TEST_ID = ?`, [testId])
      .then((data) => {
        let testDetails;
        if (data.rows.length > 0) {
          testDetails = {
            testDate: data.rows.item(0).TEST_DATE,
            subjectName: data.rows.item(0).SUBJECT_NAME,
            totalQue: data.rows.item(0).TOTAL_QUES,
            corrMarks: data.rows.item(0).CORR_MARKS,
            inCorrMarks: data.rows.item(0).INCORR_MARKS,
            obtainedMarks: data.rows.item(0).OBTAINED_MARKS,
            phyObtainedMarks: data.rows.item(0).PHY_OBTAINED_MARKS,
            chemObtainedMarks: data.rows.item(0).CHEM_OBTAINED_MARKS,
            mathsObtainedMarks: data.rows.item(0).MATHS_OBTAINED_MARKS,
            totalMarks: data.rows.item(0).TOTAL_MARKS,
            totalTime: data.rows.item(0).TOTAL_TIME,
            attQueCount: data.rows.item(0).ATTEMPTED,
            nonAttQueCount: data.rows.item(0).NON_ATTEMPTED,
            corrQueCount: data.rows.item(0).CORR_QUE,
            inCorrQueCount: data.rows.item(0).INCORR_QUE,
            timeTakenMin: data.rows.item(0).TIME_TAKEN_MIN,
            timeTakenSec: data.rows.item(0).TIME_TAKEN_SEC,
            testType: data.rows.item(0).TEST_TYPE,
            testMode: data.rows.item(0).TEST_MODE,
            questions: data.rows.item(0).QUESTIONS,
            chapterList: data.rows.item(0).CHAPTER_LIST,
            manualCount: data.rows.item(0).MANUAL_COUNT,
            phyStart: data.rows.item(0).PHY_START,
            chemStart: data.rows.item(0).CHEM_START,
            mathsStart: data.rows.item(0).MATHS_START,
            testSubmitted: data.rows.item(0).TEST_SUBMITTED,
            questionStartsAt: data.rows.item(0).QUESTION_STARTS_AT,
          };
        }
        return testDetails;
      });
  }

  getTest(tableName) {
    // console.log('in get test service')
    this.getTestDetails(tableName);
    return this.testDetailsObj.asObservable();
  }

  getLastTest(tableName) {
    this.getLastTestInfo(tableName);
    return this.lastTestDetailObj.asObservable();
  }

  getLastTestInfo(tableName) {
    return this.databaseServiceService
      .getDataBase()
      .executeSql(
        `SELECT TEST_ID, OBTAINED_MARKS, TOTAL_MARKS, TEST_TYPE, TEST_SUBMITTED
          FROM ${tableName}  WHERE TEST_SUBMITTED = 'true' ORDER BY TEST_ID DESC LIMIT 1`,
        []
      )
      .then((result) => {
        let lastTestDetailsObj;
        if (result.rows.length > 0) {
          lastTestDetailsObj = {
            testId: result.rows.item(0).TEST_ID,
            obtainedMarks: result.rows.item(0).OBTAINED_MARKS,
            totalMarks: result.rows.item(0).TOTAL_MARKS,
            testType: result.rows.item(0).TEST_TYPE,
            testSubmitted: result.rows.item(0).TEST_SUBMITTED,
          };
          this.lastTestDetailObj.next(lastTestDetailsObj);
          return lastTestDetailsObj;
        } else {
          lastTestDetailsObj = {
            testId: 0,
            obtainedMarks: 0,
            totalMarks: 0,
            testType: "",
            testSubmitted: "",
          };
          this.lastTestDetailObj.next(lastTestDetailsObj);
          return lastTestDetailsObj;
        }
      });
  }

  getSubjectWiseTest(subjectName, tableName) {
    return this.databaseServiceService
      .getDataBase()
      .executeSql(
        `SELECT SUBJECT_NAME, OBTAINED_MARKS, TOTAL_MARKS FROM ${tableName}  WHERE SUBJECT_NAME LIKE ? AND TEST_SUBMITTED = 'true'`,
        [subjectName]
      )
      .then((data) => {
        let testDetails = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            testDetails.push({
              subjetName: data.rows.item(i).SUBJECT_NAME,
              obtainedMarks: data.rows.item(i).OBTAINED_MARKS,
              totalMarks: data.rows.item(i).TOTAL_MARKS,
            });
          }
        }
        return testDetails;
      });
  }
}
