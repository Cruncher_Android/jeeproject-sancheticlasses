import { Component, OnInit } from "@angular/core";
import { Storage } from "@ionic/storage";
import { NavController } from "@ionic/angular";
import { StorageServiceService } from "./../../services/storage-service.service";

@Component({
  selector: "app-settings",
  templateUrl: "./settings.page.html",
  styleUrls: ["./settings.page.scss"],
})
export class SettingsPage implements OnInit {
  settings: {
    time: number;
    correctMarks: number;
    inCorrectMarks: number;
    questionCount: number;
    numericalCount: number;
  };

  defaultSettings: {
    time: number;
    correctMarks: number;
    inCorrectMarks: number;
    questionCount: number;
    numericalCount: number;
  };

  numNegativeMarks = 0;

  changeCount: any;

  constructor(
    private storage: Storage,
    private navController: NavController,
    private storageServiceService: StorageServiceService
  ) {}

  ngOnInit() {
    this.settings = {
      correctMarks: 0,
      inCorrectMarks: 0,
      questionCount: 0,
      time: 0,
      numericalCount: 0,
    };
  }

  ionViewWillEnter() {
    this.storage.ready().then((ready) => {
      if (ready) {
        if (this.storageServiceService.userLoggedIn == true) {
          this.storage.get("defaultSettings").then((result) => {
            this.defaultSettings = result;
            this.settings = Object.assign({}, this.defaultSettings);
          });
        } else if (this.storageServiceService.userLoggedIn == "demo") {
          this.storage.get("defaultSettingsDemo").then((result) => {
            this.defaultSettings = result;
            this.settings = Object.assign({}, this.defaultSettings);
          });
        }
      }
    });
  }

  onQueUp() {
    this.settings.questionCount++;
  }

  onQueDown() {
    if (this.settings.questionCount > 0) this.settings.questionCount--;
  }

  onNumUp() {
    this.settings.numericalCount++;
  }

  onNumDown() {
    if (this.settings.numericalCount > 10) this.settings.numericalCount--;
  }

  onTimeUp() {
    this.settings.time++;
  }

  onTimeDown() {
    if (this.settings.time > 0) this.settings.time--;
  }

  onCorrUp() {
    this.settings.correctMarks++;
  }

  onCorrDown() {
    if (this.settings.correctMarks > 0) this.settings.correctMarks--;
  }

  onIncorrUp() {
    this.settings.inCorrectMarks++;
  }

  onIncorrDown() {
    if (this.settings.inCorrectMarks > 0) this.settings.inCorrectMarks--;
  }

  onQueUpPress() {
    this.changeCount = setInterval(() => {
      this.onQueUp();
    }, 50);
  }

  onQueDownPress() {
    this.changeCount = setInterval(() => {
      this.onQueDown();
    }, 50);
  }

  onNumUpPress() {
    this.changeCount = setInterval(() => {
      this.onQueUp();
    }, 50);
  }

  onNumDownPress() {
    this.changeCount = setInterval(() => {
      this.onQueDown();
    }, 50);
  }

  onTimeUpPress() {
    this.changeCount = setInterval(() => {
      this.onTimeUp();
    }, 50);
  }

  onTimeDownPress() {
    this.changeCount = setInterval(() => {
      this.onTimeDown();
    }, 50);
  }

  onCorrUpPress() {
    this.changeCount = setInterval(() => {
      this.onCorrUp();
    }, 50);
  }

  onCorrDownPress() {
    this.changeCount = setInterval(() => {
      this.onCorrDown();
    }, 50);
  }

  onIncorrUpPress() {
    this.changeCount = setInterval(() => {
      this.onIncorrUp();
    }, 50);
  }

  onIncorrDownPress() {
    this.changeCount = setInterval(() => {
      this.onIncorrDown();
    }, 50);
  }

  onPressup() {
    clearInterval(this.changeCount);
  }

  onSet() {
    this.storage.ready().then((ready) => {
      if (ready) {
        if (this.storageServiceService.userLoggedIn == true) {
          this.storage.set("defaultSettings", this.settings).then((_) => {
            this.navController.back();
          });
        } else if (this.storageServiceService.userLoggedIn == "demo") {
          this.storage.set("defaultSettingsDemo", this.settings).then((_) => {
            this.navController.back();
          });
        }
      }
    });
  }

  onCancel() {
    this.storage.ready().then((ready) => {
      if (ready) {
        if (this.storageServiceService.userLoggedIn == true) {
          this.storage
            .set("defaultSettings", this.defaultSettings)
            .then((_) => {
              this.navController.back();
            });
        } else if (this.storageServiceService.userLoggedIn == "demo") {
          this.storage
            .set("defaultSettingsDemo", this.defaultSettings)
            .then((_) => {
              this.navController.back();
            });
        }
      }
    });
  }
}
