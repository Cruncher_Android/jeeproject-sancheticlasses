import { Storage } from "@ionic/storage";
import { PopoverController } from "@ionic/angular";
import { Component, OnInit, Input } from "@angular/core";
import { StorageServiceService } from "./../../services/storage-service.service";

@Component({
  selector: "app-change-default-settings",
  templateUrl: "./change-default-settings.component.html",
  styleUrls: ["./change-default-settings.component.scss"],
})
export class ChangeDefaultSettingsComponent implements OnInit {
  @Input() defaultSettings: {
    time: number;
    correctMarks: number;
    inCorrectMarks: number;
    questionCount: number;
    numericalCount: number;
  };

  settings: {
    time: number;
    correctMarks: number;
    inCorrectMarks: number;
    questionCount: number;
    numericalCount: number;
  };

  @Input() testMode: string;

  changeCount: any;

  numNegativeMarks = 0;

  constructor(
    private storageServiceService: StorageServiceService,
    private popoverController: PopoverController,
    private storage: Storage
  ) {}

  ngOnInit() {
    this.storage.ready().then((ready) => {
      if (ready) {
        this.storage.set("changeSettingsPopOver", true);
      }
    });
    this.settings = Object.assign({}, this.defaultSettings);
  }

  onSet() {
    this.storage.ready().then((ready) => {
      if (ready) {
        if (this.storageServiceService.userLoggedIn == true) {
          this.storage.set("defaultSettings", this.settings).then((_) => {
            this.storage.set("changeSettingsPopOver", false);
            this.popoverController.dismiss();
          });
        } else if (this.storageServiceService.userLoggedIn == "demo") {
          this.storage.set("defaultSettingsDemo", this.settings).then((_) => {
            this.storage.set("changeSettingsPopOver", false);
            this.popoverController.dismiss();
          });
        }
      }
    });
  }

  onCancel() {
    this.storage.ready().then((ready) => {
      if (ready) {
        if (this.storageServiceService.userLoggedIn == true) {
          this.storage
            .set("defaultSettings", this.defaultSettings)
            .then((_) => {
              this.storage.set("changeSettingsPopOver", false);
              this.popoverController.dismiss();
            });
        } else if (this.storageServiceService.userLoggedIn == "demo") {
          this.storage
            .set("defaultSettingsDemo", this.defaultSettings)
            .then((_) => {
              this.storage.set("changeSettingsPopOver", false);
              this.popoverController.dismiss();
            });
        }
      }
    });
  }

  onQueUp() {
    this.settings.questionCount++;
  }

  onQueDown() {
    if (this.settings.questionCount > 0) this.settings.questionCount--;
  }

  onNumUp() {
    this.settings.numericalCount++;
  }

  onNumDown() {
    if (this.settings.numericalCount > 10) this.settings.numericalCount--;
  }

  onTimeUp() {
    this.settings.time++;
  }

  onTimeDown() {
    if (this.settings.time > 0) this.settings.time--;
  }

  onCorrUp() {
    this.settings.correctMarks++;
  }

  onCorrDown() {
    if (this.settings.correctMarks > 0) this.settings.correctMarks--;
  }

  onIncorrUp() {
    this.settings.inCorrectMarks++;
  }

  onIncorrDown() {
    if (this.settings.inCorrectMarks > 0) this.settings.inCorrectMarks--;
  }

  onQueUpPress() {
    this.changeCount = setInterval(() => {
      this.onQueUp();
    }, 50);
  }

  onQueDownPress() {
    this.changeCount = setInterval(() => {
      this.onQueDown();
    }, 50);
  }

  onNumUpPress() {
    this.changeCount = setInterval(() => {
      this.onQueUp();
    }, 50);
  }

  onNumDownPress() {
    this.changeCount = setInterval(() => {
      this.onQueDown();
    }, 50);
  }

  onTimeUpPress() {
    this.changeCount = setInterval(() => {
      this.onTimeUp();
    }, 50);
  }

  onTimeDownPress() {
    this.changeCount = setInterval(() => {
      this.onTimeDown();
    }, 50);
  }

  onCorrUpPress() {
    this.changeCount = setInterval(() => {
      this.onCorrUp();
    }, 50);
  }

  onCorrDownPress() {
    this.changeCount = setInterval(() => {
      this.onCorrDown();
    }, 50);
  }

  onIncorrUpPress() {
    this.changeCount = setInterval(() => {
      this.onIncorrUp();
    }, 50);
  }

  onIncorrDownPress() {
    this.changeCount = setInterval(() => {
      this.onIncorrDown();
    }, 50);
  }

  onPressup() {
    clearInterval(this.changeCount);
  }
}
