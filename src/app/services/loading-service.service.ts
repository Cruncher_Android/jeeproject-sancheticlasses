import { Injectable } from "@angular/core";
import { LoadingController } from "@ionic/angular";

@Injectable({
  providedIn: "root",
})
export class LoadingServiceService {
  constructor(private loadingController: LoadingController) {}

  async createLoading(message) {
    const loading = await this.loadingController.create({
      animated: true,
      backdropDismiss: false,
      spinner: "bubbles",
      showBackdrop: true,
      message: message,
      keyboardClose: true,
    });
    await loading.present();
  }
}
