import { TestBed } from '@angular/core/testing';

import { OverallSummaryService } from './overall-summary.service';

describe('OverallSummaryService', () => {
  let service: OverallSummaryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OverallSummaryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
