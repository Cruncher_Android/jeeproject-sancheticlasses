import { PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-change-test-time',
  templateUrl: './change-test-time.component.html',
  styleUrls: ['./change-test-time.component.scss'],
})
export class ChangeTestTimeComponent implements OnInit {

  testTime: number = 180;
  changeCount: any;

  constructor(private storage: Storage,
              private popoverController: PopoverController) { }

  ngOnInit() {
    
  } 

  onTimeUp() {
    this.testTime++;
  }

  onTimeDown() {
    if (this.testTime > 0) this.testTime--;
  }

  onTimeUpPress() {
    this.changeCount = setInterval(() => {
      this.onTimeUp();
    }, 50);
  }

  onTimeDownPress() {
    this.changeCount = setInterval(() => {
      this.onTimeDown();
    }, 50);
  }

  onPressup() {
    clearInterval(this.changeCount);
  }

  onSet() {
    this.storage.ready().then(ready => {
      if(ready) {
        this.storage.set('testTime', this.testTime).then(_ => {
          this.popoverController.dismiss();
        })
      }
    })
  }

  onCancel() {
    this.storage.ready().then(ready => {
      if(ready) {
        this.storage.set('testTime', 0).then(_ => {
          this.popoverController.dismiss();
        })
      }
    })
  }
}
