import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PerformanceGraphPageRoutingModule } from './performance-graph-routing.module';

import { PerformanceGraphPage } from './performance-graph.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PerformanceGraphPageRoutingModule
  ],
  declarations: [PerformanceGraphPage]
})
export class PerformanceGraphPageModule {}
