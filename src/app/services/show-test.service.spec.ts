import { TestBed } from '@angular/core/testing';

import { ShowTestService } from './show-test.service';

describe('ShowTestService', () => {
  let service: ShowTestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ShowTestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
