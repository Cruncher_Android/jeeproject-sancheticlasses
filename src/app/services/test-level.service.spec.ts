import { TestBed } from '@angular/core/testing';

import { TestLevelService } from './test-level.service';

describe('TestLevelService', () => {
  let service: TestLevelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestLevelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
