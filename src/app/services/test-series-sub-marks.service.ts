import { Injectable } from '@angular/core';
import { DatabaseServiceService } from './database-service.service';

@Injectable({
  providedIn: 'root'
})
export class TestSeriesSubMarksService {

  constructor(private databaseServiceService: DatabaseServiceService) { }

  createTable() {
    this.databaseServiceService.getDatabaseState().subscribe( ready => {
      if (ready) {
        this.databaseServiceService.getDataBase().executeSql(`CREATE TABLE IF NOT EXISTS testSeries_sub_marks
        (sub_id integer primary key,test1 integer,test2 integer ,test3 integer,test4 integer,test5 integer)`, []);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testSeries_sub_marks(sub_id,test1,test2,test3,
          test4,test5)values(?,?,?,?,?,?)`,[1,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testSeries_sub_marks(sub_id,test1,test2,test3,
          test4,test5)values(?,?,?,?,?,?)`,[2,0,0,0,0,0]);
        this.databaseServiceService.getDataBase().executeSql(`INSERT OR IGNORE INTO testSeries_sub_marks(sub_id,test1,test2,test3,
          test4,test5)values(?,?,?,?,?,?)`,[3,0,0,0,0,0]);
      }
    });
  }
}
