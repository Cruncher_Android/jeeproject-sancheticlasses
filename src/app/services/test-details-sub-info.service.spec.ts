import { TestBed } from '@angular/core/testing';

import { TestDetailsSubInfoService } from './test-details-sub-info.service';

describe('TestDetailsSubInfoService', () => {
  let service: TestDetailsSubInfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestDetailsSubInfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
