import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CareerNotesPage } from './career-notes.page';

const routes: Routes = [
  {
    path: '',
    component: CareerNotesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CareerNotesPageRoutingModule {}
