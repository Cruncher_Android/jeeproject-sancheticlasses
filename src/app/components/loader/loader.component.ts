import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
})
export class LoaderComponent implements OnInit {

  loader: number = 0;
  loaderPer:number = 0;
  loaderTime;
  loadingMin;
  loadingSec;
  totalSec = 300;
  constructor() { }

  ngOnInit() {
    this.loaderTime = 0;
    let stopClockInterval = setInterval(_ => {
      this.loaderTime++
      this.totalSec--;
      this.loadingMin = Math.floor(this.totalSec / 60);
      this.loadingSec = Math.floor(this.totalSec % 60);
      if(this.loadingMin <= 9) {
        this.loadingMin = '0' + this.loadingMin;
      }
      if(this.loadingSec <= 9) {
        this.loadingSec = '0' + this.loadingSec;
      } 

      if(this.loaderTime % 3 == 0) {
        this.loader = this.loader + 0.01;
      }
      this.loaderPer = Math.floor(this.loader * 100);
      // console.log('loaderPer', this.loaderPer);
      if(this.loaderTime == 300) {
        clearTimeout(stopClockInterval);
      }
    }, 1000)
  }
}
